### Product Catalog:

This project is an exemple of a product-category module based on a simple web application using Entity Framework and the MVC design pattern.
The solution contains a consolApp added in the beginning, just for testing and manipulate database.

### Class Diagram:

this figure below describe the dependency and association between the solution's layers(models, service, DAO, ...)
![Class Diagram](https://bitbucket.org/aabdelmounaim/product-catalog/raw/9bb41519707d7b385333e8c16b912b3541673137/CatalogProductDiagram.jpg)

### Architecture:

![Class Diagram](https://bitbucket.org/aabdelmounaim/product-catalog/raw/9bb41519707d7b385333e8c16b912b3541673137/architecture.jpg)
