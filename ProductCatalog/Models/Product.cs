﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Models
{
    [Table("PRODUCTS")]
    public class Product
    {
        [Key]
        [Display(Name ="ID")]
        public int ProductId { get; set; }
        [StringLength(50), Required]
        public string Designation { get; set; }
        [Range(0, 1000000), Required]
        public double Price { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
    }
}
