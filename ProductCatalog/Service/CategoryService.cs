﻿using Microsoft.AspNetCore.Http;
using ProductCatalog.Interfaces;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Service
{
    public class CategoryService : ICategoryService
    {
        CatalogDataBaseContext DbContext { get; set; }
        public CategoryService(CatalogDataBaseContext DbCtx)
        {
            this.DbContext = DbCtx;
        }
        public void Delete(int Id)
        {
            Category c = DbContext.Categories.Find(Id);
            DbContext.Categories.Remove(c);
            DbContext.SaveChanges();
        }

        public IEnumerable<Category> FindAll()
        {
            return DbContext.Categories.ToList();
        }


        public IEnumerable<Category> FindByName(string keyword)
        {
            return DbContext.Categories.Where(c=>c.Name.Contains(keyword)).ToList();
        }

        public Category GetOne(int Id)
        {
            return DbContext.Categories.Find(Id);
        }

        public Category Save(Category categ)
        {
            DbContext.Categories.Add(categ);
            DbContext.SaveChanges();
            return categ;
        }

        public void Update(IFormCollection form)
        {
            int id = Convert.ToInt32(form["Id"]);
            Category c = DbContext.Categories.Find(id);
            c.Name = form["Name"];
            DbContext.Categories.Update(c);
            DbContext.SaveChanges();
        }
    }
}
