﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Interfaces;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Service
{
    public class ProductService : IProductService
    {
        CatalogDataBaseContext DbContext { get; set; }

        public ProductService(CatalogDataBaseContext DbCtx)
        {
            this.DbContext = DbCtx;
        }

        public void Delete(int Id)
        {
            Product p = DbContext.Products.Find(Id);
            DbContext.Products.Remove(p) ;
            DbContext.SaveChanges();
        }

        public IEnumerable<Product> FindAll()
        {
            return DbContext.Products.Include(prod => prod.Category).ToList();
        }

        public IEnumerable<Product> FindByDesignation(string keyword)
        {
            return DbContext.Products.Where(p=>p.Designation.Contains(keyword)).ToList();
        }

        public Product GetOne(int Id)
        {
            return DbContext.Products.Find(Id);
        }

        public Product Save(Product p)
        {
            DbContext.Products.Add(p);
            DbContext.SaveChanges();
            return p;
        }

        public void Update(IFormCollection form)
        {
            int id = Convert.ToInt32(form["Id"]);
            Product prod = DbContext.Products.Find(id);
            prod.Designation = form["Designation"];
            prod.Price = Convert.ToDouble(form["Price"]);
            prod.CategoryId = Convert.ToInt32(form["CategoryId"]);
            DbContext.Products.Update(prod);
            DbContext.SaveChanges();
        }
    }
}
