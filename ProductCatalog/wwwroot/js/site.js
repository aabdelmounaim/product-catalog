﻿// Write your Javascript code.
$(document).on("click", ".UpdateProductDialog", function () {
    var id = $(this).data('prod-id');
    var designation = $(this).data('desig');
    var price = $(this).data('price');
    var category = $(this).data('categ');
    $("#modal-body-update-product #updatedProductId").val(id);
    $("#modal-body-update-product #updatedDesignation").val(designation);
    $("#modal-body-update-product #updatedPrice").val(price);
    $("#modal-body-update-product #updatedCategoryId").val(category);
});

$(document).on("click", ".DeleteProductDialog", function () {
    var productId = $(this).data('id');
    $("#modal-body-delete-product #DeletedProductId").val(productId);
    $("#modal-body-delete-product #DeletedProductId2").text(productId);
});

$(document).on("click", ".UpdateCategoryDialog", function () {
    var id = $(this).data('categ-id');
    var name = $(this).data('name');
    $("#modal-body-update-category #updatedCategoryId").val(id);
    $("#modal-body-update-category #updatedName").val(name);
});

$(document).on("click", ".DeleteCategoryDialog", function () {
    var id = $(this).data('id');
    $("#modal-body-delete-category #DeletedCategoryId").val(id);
    $("#modal-body-delete-category #DeletedCategoryId2").text(id);
});
