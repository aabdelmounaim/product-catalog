﻿using Microsoft.AspNetCore.Http;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Interfaces
{
    public interface IProductService
    {
        Product Save(Product p);
        IEnumerable<Product> FindAll();
        IEnumerable<Product> FindByDesignation(string keyword);
        Product GetOne(int Id);
        void Delete(int Id);
        void Update(IFormCollection form);

    }
}
