﻿using Microsoft.AspNetCore.Http;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Interfaces
{
    public interface ICategoryService
    {
        Category Save(Category categ);
        IEnumerable<Category> FindAll();
        IEnumerable<Category> FindByName(string keyword);
        Category GetOne(int Id);
        void Delete(int Id);
        void Update(IFormCollection form);
    }
}
