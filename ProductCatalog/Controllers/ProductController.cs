﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Interfaces;
using ProductCatalog.Models;
using ProductCatalog.Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductCatalog.Controllers
{
    public class ProductController : Controller
    {

        private readonly IProductService ProductService;
        private readonly ICategoryService CategoryService;

        public ProductController(IProductService ProductService,
                                 ICategoryService CategoryService)
        {
            this.ProductService = ProductService;
            this.CategoryService = CategoryService;
        }
        

        // GET: /<controller>/
        public IActionResult Index(int page = 0, int size = 5, string keyword = "")
        {
            int pos = page * size;
            if (keyword == null) keyword = "";
            int totalProduct = ProductService.FindAll().Where(p => p.Designation.Contains(keyword)).Count();
            int totalPages = totalProduct / size;
            if (totalProduct%size!=0) {
                totalPages += 1;
            }
            ViewBag.CurrentPage = page;
            ViewBag.totalPages = totalPages;
            IEnumerable<Category> categories = CategoryService.FindAll();
            ViewBag.categories = categories;
            ViewBag.keyword = keyword;
            IEnumerable<Product> products = ProductService
                                            .FindAll()
                                            .Where(p=>p.Designation.Contains(keyword))
                                            .Skip(pos)
                                            .Take(size)
                                            .ToList();
            return View("Products", products);
        }

        public IActionResult ProductForm()
        {
            Product prod = new Product();
            IEnumerable<Category> categories = CategoryService.FindAll().ToList();
            ViewBag.categories = categories;
            return View("ProductForm", prod);

        }
        [HttpPost]
        public IActionResult Save(Product prod)
        {
            
            if (ModelState.IsValid) {
                ProductService.Save(prod);
                return RedirectToAction("Index");
            }
            IEnumerable<Category> categories = CategoryService.FindAll().ToList();
            ViewBag.categories = categories;
            return View("ProductForm", prod);

        }

        [HttpPost]
        public ActionResult DeleteConfirmed(IFormCollection  form)
        {
            int id = Convert.ToInt32(form["DeletedProductId"]);
            ProductService.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Update(IFormCollection form)
        {
            ProductService.Update(form);
            return RedirectToAction("Index");
        }
    }
}
