﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.Interfaces;
using ProductCatalog.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductCatalog.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService CategoryService;

        public CategoryController(ICategoryService CategoryService)
        {
            this.CategoryService = CategoryService;
        }

        // GET: /<controller>/
        public IActionResult Index(int page = 0, int size = 5)
        {
            int pos = page * size;
            int totalCategory = CategoryService.FindAll().Count();
            int totalPages = totalCategory / size;
            if (totalCategory % size != 0)
            {
                totalPages += 1;
            }
            ViewBag.CurrentPage = page;
            ViewBag.totalPages = totalPages;
            IEnumerable<Category> categories = CategoryService
                                                .FindAll()
                                                .Skip(pos)
                                                .Take(size)
                                                .ToList();
            return View("Categories", categories);
        }

        public IActionResult CategoryForm()
        {
            Category c = new Category();
            return View("CategoryForm", c);

        }

        [HttpPost]
        public IActionResult SaveCategory(Category c)
        {

            if (ModelState.IsValid)
            {
                CategoryService.Save(c);
                return RedirectToAction("Index");
            }
            return View("CategoryForm", c);

        }

        [HttpPost]
        public ActionResult DeleteCategory(IFormCollection form)
        {
            int id = Convert.ToInt32(form["DeletedCategoryId"]);
            CategoryService.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateCategory(IFormCollection form)
        {
            CategoryService.Update(form);
            return RedirectToAction("Index");
        }
    }
}
