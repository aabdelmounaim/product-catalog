﻿using ProductCatalog.Models;
using ProductCatalog.Service;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new CatalogDataBaseContext())
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
                db.Categories.Add(
                    new Category { Name = "Computers" }
                    );
                db.Categories.Add(
                    new Category { Name = "Phones" }
                    );
                db.Categories.Add(
                    new Category { Name = "Printers" }
                    );
                db.Categories.Add(
                    new Category { Name = "Modems" }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP 8470p G3",
                        Price = 2800,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP 8470p G3",
                        Price = 2800,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP 6570b G3",
                        Price = 3000,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "DELL 5480 i3 Tactile G7",
                        Price = 4800,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "LENOVO IdeaPad 320  ",
                        Price = 3800,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "DELL E7440 I5",
                        Price = 3950,
                        CategoryId = 1
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP DeskJet Ink Advantage 2135",
                        Price = 600,
                        CategoryId = 3
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP LaserJet Pro M12a",
                        Price = 1350,
                        CategoryId = 3
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "HP LaserJet Pro M130nw",
                        Price = 2250,
                        CategoryId = 3
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "Apple IPhone 7 32Go SILVER ",
                        Price = 5319,
                        CategoryId = 2
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "Samsung Galaxy J4",
                        Price = 1645,
                        CategoryId = 2
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "Samsung Galaxy J8 4Go RAM 64 Go",
                        Price = 3197,
                        CategoryId = 2
                    }
                    );
                db.Products.Add(
                    new Product
                    {
                        Designation = "MTC INFINITY Android  Black",
                        Price = 789,
                        CategoryId = 2
                    }
                    );
                db.SaveChanges();
            }
        }
    }
}
